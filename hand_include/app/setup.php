<?php //include anywhere

add_action('wp_enqueue_scripts', function () {
    wp_enqueue_script('sage/ajax.js', asset_path('scripts/ajax.js'), ['jquery'], null, true);
    $ajaxurl = '';
    if (in_array('sitepress-multilingual-cms/sitepress.php', get_option('active_plugins'))) {
        $ajaxurl .= admin_url('admin-ajax.php?lang=' . ICL_LANGUAGE_CODE);
    } else {
        $ajaxurl .= admin_url('admin-ajax.php');
    }
    $params = [
        'ajaxurl' => $ajaxurl,
        'ajaxnonce' => wp_create_nonce("sstring")
    ];
    wp_localize_script('sage/ajax.js', 'params', $params);
}, 101);
