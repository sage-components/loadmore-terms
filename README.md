# Ajax load more for Sage/roots + wordpress + jQuery

## Dependencies
 Foundation, jQuery
    
## How to set up
1) use git merge
2) place everything from `hand_include/` to its place
#### Blade usage
``` 
@include('components/blog-posts', [
  'initial'=> How much posts should be loaded initialy
  'btn_text'=> buton text, empty for hidden
  'on_one_click'=> How much posts should be loaded on one btn click
  'category'=> which category only should be loaded
]
```

#### Other info
Author: Zeni
