jQuery(document).ready(function () {
  //page ready - initial load posts
  var category = 0;

  //if we are on blog page (with categories filter)
  if ($('.categories-filter').length) {
    category = window.location.hash.replace(/[^a-z]/gi, '');
    $('#blog-posts').attr("data-category", category)

    $('div.categories-filter a').each(function () {
      setActive(this, category);
      $(this).click(function () {
        category = $(this).attr("href").replace(/[^a-z]/gi, '');
        clearPosts();
        loadPost(category);
        $('div.categories-filter a').each(function () {
          setActive(this, category);
        });
      })
    })
  }

  $('button.load-more').on('click', function (e) {
    e.preventDefault();
    $(this).prop( "disabled", true );
    loadPost(category);
  });

  function loadPost(category) {
    $.ajax({
      url: params.ajaxurl,
      method: 'POST',
      dataType: 'html',
      data: {
        action: 'load_posts',
        security: params.ajaxnonce,
        one_click: $('#blog-posts').attr("data-on-one-click"),
        num_showing: $('[data-here]').children('div').length,
        category: category,
        post_type: $('#blog-posts').attr("data-post-type"),
      },
      success: function (data) {
        $('[data-here]').append(data);
        var num_showing = $('[data-here]').children('div').length;
        var max = $('#blog-posts .grid-item').last().attr("data-max");
        if (num_showing >= max) {
          $('button.load-more').hide();
        } else {
          $('button.load-more').show();
        }
      },
      complete: function () {
        $('button.load-more').prop("disabled", false);
      },
    });
  }

  function clearPosts() {
    $('[data-here]').empty();
  }

  function setActive(el, category) {
    if (category == $(el).attr("href").replace(/[^a-z]/gi, '')) {
      $(el).addClass("active");
    } else {
      $(el).removeClass("active");
    }
  }

});
