{{--
  args:
  'post'=> [WP_object] $post - wordpredd get_post() value
--}}
<a href="{{get_permalink($post)}}" title="{{$post->post_title}}">
  <div class="card">
      @include('components.img', [
      'url' => get_the_post_thumbnail_url($post)
      ])
    <div class="card-section">
      <h1 class="h1 h1--green">{{$post->post_title}}</h1><br>
      <p>
        {{wp_trim_words($post->post_content, 55, "...")}}
      </p>
      @include('components.img', [
          'path' => "images/arrow.svg",
          'class' => "arrow"
          ])
    </div>
  </div>
</a>
