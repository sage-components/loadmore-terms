{{-- blade args:
  'initial'=> How much posts should be loaded initialy
  'btn_text'=> buton text, empty for hidden
  'on_one_click'=> How much posts should be loaded on one btn click
  'category'=> which category only should be loaded
  'post_type'=> post type of content posts
--}}

{{-- Define variables --}}
@empty($on_one_click)
  @php($on_one_click=3)
@endempty
@empty($initial)
  @php($initial=5)
@endempty

{{-- Category Filter --}}
<div class="grid-container categories-filter">
  <div class="grid-x">
    <div class="cell">
        <span class="h2 h2--green">{{$news_fields['categories']}}</span>
        <a href="#" class="active">All</a>
        @foreach(get_categories() as $category)
          <a href="#{{$category->slug}}"> {{$category->name}} </a>
        @endforeach
    </div>
  </div>
</div>

  {{-- blog posts grid --}}
<div id="blog-posts"
  data-num-showing="{{$initial}}"
  data-on-one-click="{{$on_one_click}}"
  data-category="{{$category}}"
  data-post-type="{{$post_type}}"
>
  <div class="grid-container">
    <div class="grid-x">
      <div class="cell">
        <div class="grid" data-here>
          @php($posts = get_posts([
            'numberposts' => $initial,
            'post_type'=> $post_type
            ]))
          @foreach($posts as $key=>$post)
          <div class="grid-item">
            @include('components.blog-post', [
              'post'=> $post,
              ])
          </div>
        @endforeach
      </div>
    </div>
    </div>
  </div>
</div>

<div class="grid-container">
  @isset($btn_text)
  @include('components/btn-load-more', [
    'text'=>$btn_text
    ])
@endisset
</div>