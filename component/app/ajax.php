<?php

namespace App;

class Ajax
{
    private $category;
    private $offset;
    private $one_click;
    private $post_type;

    public function __construct()
    {
        $this->category = sanitize_text_field($this->getPostAttribute('category'));
        $this->offset = (int)$this->getPostAttribute('num_showing');
        $this->one_click = (int)$this->getPostAttribute('one_click');
        $this->post_type = sanitize_text_field($this->getPostAttribute('post_type'));

        add_action('wp_ajax_load_posts', [$this, 'loadPosts']);
        add_action('wp_ajax_nopriv_load_posts', [$this, 'loadPosts']);
    }

    public function loadPosts()
    {
        check_ajax_referer('sstring', 'security');
        $count = $this->countAllPosts();
        $posts = get_posts([
            'category_name' => esc_textarea($this->category),
            'offset' => $this->offset,
            'numberposts' => $this->one_click,
            'exclude' => [get_the_ID()], //if used inside post - remove current
            'post_type' => $this->post_type
        ]);

        foreach ($posts as $key => $post) {
            echo '<div data-max="'. $count .'">';
            echo Template('components.blog-post', [
                    'post'=> $post
                    ]);
            echo '</div>';
        }
        die;
        wp_die();
    }

    /**
     * How many posts can I get ?
     *
     * @return integer
     */
    private function countAllPosts()
    {
        $count = get_category_by_slug($this->category)->category_count;
        if (!$count) {
            $count = (int)wp_count_posts()->publish;
        }
        return $count;
    }

    private function getPostAttribute($index)
    {
        if (isset($_POST[$index])) {
            return $_POST[$index];
        }

        return null;
    }
}

new Ajax();
